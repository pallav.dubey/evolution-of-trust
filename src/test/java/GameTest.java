import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.PrintStream;

public class GameTest {

    @Test
    public void shouldPrintScoreForOneRound() {

        Player playerOne = Mockito.mock(Player.class);
        Mockito.when(playerOne.move()).thenReturn(MoveType.COOPERATE);

        Player playerTwo = Mockito.mock(Player.class);
        Mockito.when(playerTwo.move()).thenReturn(MoveType.COOPERATE);

        Machine machine = Mockito.mock(Machine.class);
        Mockito.when(machine.score(Mockito.any(MoveType.class), Mockito.any(MoveType.class)))
                .thenReturn(new Score(2,2));

        PrintStream printStream = Mockito.mock(PrintStream.class);
        Score expectedScore = new Score(2,2);

        Game game = new Game(playerOne, playerTwo, machine, printStream);

        game.playSingleRound();

        Mockito.verify(playerOne, Mockito.times(1)).move();
        Mockito.verify(playerOne, Mockito.times(1)).addScore(Mockito.anyInt());
        Mockito.verify(playerTwo, Mockito.times(1)).move();
        Mockito.verify(playerTwo, Mockito.times(1)).addScore(Mockito.anyInt());
        Mockito.verify(machine,  Mockito.times(1))
                .score(Mockito.any(MoveType.class), Mockito.any(MoveType.class));
        Mockito.verify(printStream, Mockito.times(1)).println(expectedScore.toString());
    }

    @Test
    public void shouldPrintScoreMultipleRounds() {
        Player playerOne = Mockito.mock(Player.class);
        Mockito.when(playerOne.move()).thenReturn(MoveType.COOPERATE);

        Player playerTwo = Mockito.mock(Player.class);
        Mockito.when(playerTwo.move()).thenReturn(MoveType.COOPERATE);

        Machine machine = Mockito.mock(Machine.class);
        Mockito.when(machine.score(Mockito.any(MoveType.class), Mockito.any(MoveType.class)))
                .thenReturn(new Score(2,2));

        PrintStream printStream = Mockito.mock(PrintStream.class);
        Score expectedScore = new Score(2,2);

        Game game = new Game(playerOne, playerTwo, machine, printStream);

        game.play(3);

        Mockito.verify(playerOne, Mockito.times(3)).move();
        Mockito.verify(playerOne, Mockito.times(3)).addScore(Mockito.anyInt());
        Mockito.verify(playerTwo, Mockito.times(3)).move();
        Mockito.verify(playerTwo, Mockito.times(3)).addScore(Mockito.anyInt());
        Mockito.verify(machine,  Mockito.times(3))
                .score(Mockito.any(MoveType.class), Mockito.any(MoveType.class));
        Mockito.verify(printStream, Mockito.times(3)).println(expectedScore.toString());

        Mockito.verify(printStream, Mockito.times(1))
                .println(playerOne.getCumulativeScore() + " " + playerTwo.getCumulativeScore());

    }
}
