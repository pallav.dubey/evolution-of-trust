import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MachineTest {

    @Test
    public void shouldReturnCooperateScore() {
        Score expected = new Score(2,2);
        Machine machine = new Machine();

        Score actual = machine.score(MoveType.COOPERATE, MoveType.COOPERATE);

        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnCheatScoreWhenPlayerTwoCheats() {
        Score expected =  new Score(-1, 3);
        Machine machine = new Machine();

        Score actual = machine.score(MoveType.COOPERATE, MoveType.CHEAT);

        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnCheatScoreWhenPlayerOneCheats() {
        Score expected =  new Score(3, -1);
        Machine machine = new Machine();

        Score actual = machine.score(MoveType.CHEAT, MoveType.COOPERATE);

        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnScoreWhenBothPlayerCheat() {
        Score expected =  new Score(0, 0);
        Machine machine = new Machine();

        Score actual = machine.score(MoveType.CHEAT, MoveType.CHEAT);

        assertEquals(expected, actual);    }

}
