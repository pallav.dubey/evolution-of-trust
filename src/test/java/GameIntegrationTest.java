import org.junit.Assert;
import org.junit.Test;

import java.io.PrintStream;
import java.util.Scanner;

public class GameIntegrationTest {

    @Test
    public void shouldPrintCumulativeScoreForBothPlayers(){

        PlayerBehaviour consoleBehaviour = new ConsoleBehavior(new Scanner("1 \n 1 \n 1 \n 1 \n 1 \n1 \n"));
        Player playerOne = new Player(consoleBehaviour);
        Player playerTwo = new Player(consoleBehaviour);

        Game game = new Game(playerOne, playerTwo, new Machine(), new PrintStream(System.out));
        game.play(3);

        Assert.assertEquals(6, playerOne.getCumulativeScore());
        Assert.assertEquals(6, playerTwo.getCumulativeScore());
    }

    @Test
    public void shouldPrintCumulativeScoreForBothPlayersMixedInputs(){
        PlayerBehaviour consoleBehaviour = new ConsoleBehavior(new Scanner("1 \n 1 \n 2 \n 1 \n 1 \n2 \n"));
        Player playerOne = new Player(consoleBehaviour);
        Player playerTwo = new Player(consoleBehaviour);

        Game game = new Game(playerOne, playerTwo, new Machine(), new PrintStream(System.out));
        game.play(3);

        Assert.assertEquals(4, playerOne.getCumulativeScore());
        Assert.assertEquals(4, playerTwo.getCumulativeScore());
    }

    @Test
    public void shouldPrintCumulativeScoreForBothPlayersWithOneCoolPlayer(){
        PlayerBehaviour consoleBehaviour = new ConsoleBehavior(new Scanner("1 \n 1 \n 2"));
        Player playerOne = new Player(consoleBehaviour);

        PlayerBehaviour coolBehaviour =  PlayerBehaviour.ALWAYS_COOPERATE;
        Player coolPlayer = new Player(coolBehaviour);

        Game game = new Game(playerOne, coolPlayer, new Machine(), new PrintStream(System.out));
        game.play(3);

        Assert.assertEquals(7, playerOne.getCumulativeScore());
        Assert.assertEquals(3, coolPlayer.getCumulativeScore());
    }

    @Test
    public void shouldPrintCumulativeScoreForBothPlayersWithCheatPlayer(){
        PlayerBehaviour consoleBehaviour = new ConsoleBehavior(new Scanner("1 \n 1 \n 2"));
        Player playerOne = new Player(consoleBehaviour);

        PlayerBehaviour cheatBehaviour =  PlayerBehaviour.ALWAYS_CHEAT;
        Player cheatPlayer = new Player(cheatBehaviour);

        Game game = new Game(playerOne, cheatPlayer, new Machine(), new PrintStream(System.out));
        game.play(3);

        Assert.assertEquals(-2, playerOne.getCumulativeScore());
        Assert.assertEquals(6, cheatPlayer.getCumulativeScore());
    }

    @Test
    public void shouldPrintCumulativeScoreForBothCooperativeAndCheatPlayer(){
        PlayerBehaviour cooperativeBehaviour =  PlayerBehaviour.ALWAYS_COOPERATE;
        Player cooperativePlayer = new Player(cooperativeBehaviour);

        PlayerBehaviour cheatBehaviour =  PlayerBehaviour.ALWAYS_CHEAT;
        Player cheatPlayer = new Player(cheatBehaviour);

        Game game = new Game(cooperativePlayer, cheatPlayer, new Machine(), new PrintStream(System.out));
        game.play(3);

        Assert.assertEquals(-3, cooperativePlayer.getCumulativeScore());
        Assert.assertEquals(9, cheatPlayer.getCumulativeScore());
    }
}
