import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class ConsoleBehaviorTest {

    @Test
    public void shouldReturnMoveFromScanner(){
        ConsoleBehavior consoleBehavior = new ConsoleBehavior(new Scanner("1"));
        Assert.assertEquals( MoveType.COOPERATE, consoleBehavior.move());
    }
}
