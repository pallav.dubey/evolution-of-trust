import java.io.PrintStream;

public class Game {

    private Player playerOne;
    private Player playerTwo;
    private Machine machine;
    private PrintStream printStream;

    public Game(Player playerOne, Player playerTwo, Machine machine, PrintStream printStream) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.machine = machine;
        this.printStream = printStream;
    }

    public void playSingleRound() {
        Score score = machine.score(playerOne.move(), playerTwo.move());
        playerOne.addScore(score.getPlayerOneScore());
        playerTwo.addScore(score.getPlayerTwoScore());
        printStream.println(score.toString());
    }

    public void play(int round) {
        for (int i = 0; i < round; i++) {
            playSingleRound();
        }
        printStream.println(playerOne.getCumulativeScore() + " " + playerTwo.getCumulativeScore());
    }
}
