public interface PlayerBehaviour {
    PlayerBehaviour ALWAYS_COOPERATE = ()-> MoveType.COOPERATE;
    PlayerBehaviour ALWAYS_CHEAT = ()-> MoveType.CHEAT;
    MoveType move();
}
