import java.util.Scanner;

public class Player {

    private PlayerBehaviour playerBehaviour;
    private int cumulativeScore = 0;

    public Player(PlayerBehaviour playerBehaviour) {
        this.playerBehaviour = playerBehaviour;
    }

    public int getCumulativeScore() {
        return cumulativeScore;
    }

    public MoveType move() {
        return this.playerBehaviour.move();
    }

    public int addScore(int score) {
        return cumulativeScore += score;
    }
}
