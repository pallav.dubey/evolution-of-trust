enum MoveType {
    COOPERATE(1), CHEAT(2), INVALID_MOVE(0);


    private int value;

    MoveType(int value) {
        this.value = value;
    }

    public static MoveType getMoveType(int value){

        if(MoveType.COOPERATE.value == value)
            return MoveType.COOPERATE;
        else if (MoveType.CHEAT.value == value)
            return MoveType.CHEAT;
        else
            return MoveType.INVALID_MOVE;

    }
}
