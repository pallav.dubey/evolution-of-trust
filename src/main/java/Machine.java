import com.sun.tools.javac.code.Attribute;

public class Machine {
    public Score score(MoveType playerOneMove, MoveType playerTwoMove) {

        if(playerOneMove == MoveType.CHEAT && playerTwoMove == MoveType.COOPERATE)
            return new Score(3,-1);

        else if(playerOneMove == MoveType.COOPERATE && playerTwoMove == MoveType.CHEAT)
            return new Score(-1,3);

        else if(playerOneMove == MoveType.CHEAT && playerTwoMove == MoveType.CHEAT)
            return new Score(0,0);

        else
            return new Score(2,2);
    }
}
